$(function() {
    var sbula = $(".filter li a");
    sbula.click(function(e) {
        sbula.removeClass("active").not(sbula).add(this).toggleClass("active");
    });
});
$(function(){
   $('a[href^="#"]').click(function(){
        var target = $(this).attr('href');
        $('html, body').animate({scrollTop: $(target).offset().top}, 300);
        return false; 
   }); 
});
$(document).ready(function(){
	var h_hght = 347; // высота шапки
  	var h_mrg = 0;     // отступ когда шапка уже не видна
  	$(function(){
   	$(window).scroll(function(){
      	var top = $(this).scrollTop();
      	var elem = $('.filter_fixed');
      	if (top+h_mrg < h_hght) {
     	 	elem.css('top', (h_hght-top));
      	}
      	else {
       		elem.css('top', h_mrg);
      	}
    });
  });
})
$(document).ready(function(){
  $('.photo_gallery a img').click(function(){
    $('.map').addClass("z-index");
  })
  $('.lightboxOverlay').click(function(){
    $('.map').removeClass("z-index");
  })
  $('.lightbox').click(function(){
    $('.map').removeClass("z-index");
  })
  $('.lb-close').click(function(){
    $('.map').removeClass("z-index");
  })
  var winH = $(window).height();
   var winW = $(window).width();
    $('#call_back .popup-content').css('left', winW/2-$('#call_back .popup-content').width()/2);
    $('#registration .popup-content').css('left', winW/2-$('#registration .popup-content').width()/2);
    $('#registration2 .popup-content').css('left', winW/2-$('#registration2 .popup-content').width()/2);
    $('#speaker1 .popup-content').css('left', winW/2-$('#speaker1 .popup-content').width()/2);
    $('#ask_question .popup-content').css('left', winW/2-$('#ask_question .popup-content').width()/2);
    $('#enter .popup-content').css('left', winW/2-$('#enter .popup-content').width()/2);
    $('#forgot_psw .popup-content').css('left', winW/2-$('#forgot_psw .popup-content').width()/2);
        PopUpHide();
        $('.popup').click(function(e){
            $(this).fadeOut(700);
        });
        $('.popup-content').click(function(e){
            e.stopPropagation();
        });
        $('.forgot_psw').click(function() {
            $("#enter").fadeOut(500);
        });
});
        function PopUpShowEnter(){
          $("#enter").fadeIn(700)
        };
        function PopUpShowForgot(){
          $("#forgot_psw").fadeIn(700)
        };
        function PopUpShowCallBack(){
          $("#call_back").fadeIn(700)
        };
        function PopUpShowRegistration(){
          $("#registration").fadeIn(700)
        };
        function PopUpShowRegistration2(){
          $("#registration2").fadeIn(700)
        };
        function PopUpShowAskQuestion(){
          $("#ask_question").fadeIn(700)
        };
        function PopUpShowSpeaker1(){
          $("#speaker1").fadeIn(700)
        };
        function PopUpHide(){
            call_back = $("#call_back")
            call_back.hide()
            registration = $("#registration")
            registration.hide()
            registration2 = $("#registration2")
            registration2.hide()
            ask_question = $("#ask_question")
            ask_question.hide()
            speaker1 = $("#speaker1")
            speaker1.hide()
            enter = $("#enter")
            enter.hide()
            forgot_psw = $("#forgot_psw")
            forgot_psw.hide()
        };
        function PopUpFade(){
            a = $(".popup")
            a.fadeOut()
        };




            $('#search').keyup(function(){
    var searchField = $('#search').val();
    var myExp = new RegExp(searchField, 'i');

    $.getJSON('js/data.json', function(data){
        var output = '<ul class="events">';
        $.each(data, function(key, val){
            if(val.text.search(myExp) != -1) {
                output +='<li>';
                output +='<h6>' + val.pub_date + '</h6>';
                output +='<span>' + val.category + '</span>';
                output +='<p>' + val.text + '</p>';
                output +='</li>';
            }
        });
        output += '</ul>';
        $('#update').html(output).showFive();
    });

    function showFive(){
        $('#update .events li:gt(5n)').hide();
    };
});

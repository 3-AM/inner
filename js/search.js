$('#search').keyup(function(){
	var searchField = $('#search').val();
	var myExp = new RegExp(searchField, 'i');
	$.getJSON('data.json', function(data){
		var output = '<ul class="events">';
		$.each(data, function(key, val){
			if(val.text.search(myExp) != -1) {
				output +='<li>';
				output +='<h6' + val.pub_date + '</h6>';
				output +='<span>' + val.category + '</span>';
				output +='<p>' + val.text + '</p>';
				output +='</li>';
			}
		});
		output += '</ul>';
		$('#update').html(output);
	});
});
